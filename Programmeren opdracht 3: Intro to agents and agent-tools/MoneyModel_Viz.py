from MoneyModel import *
from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import ChartModule


def agent_portrayal(agent):
    # Draw each agent as a red, filled circle which fills half of each cell
    portrayal = {"Shape": "circle",
                 "Filled": "true",
                 "Layer": 0,
                 "Color": "red",
                 "r": 0.5}
    return portrayal


# Instantiate a canvas grid with its width and height in cells, and in pixels
grid = CanvasGrid(agent_portrayal, 10, 10, 500, 500)

# Instantiate a canvas grid with a
# Label (which must match the name of a model-level variable collected by the DataCollector) and a Color name
chart = ChartModule([{"Label": "Gini",
                      "Color": "Black"}],
                    data_collector_name='datacollector')

# Create and launch the  server
server = ModularServer(MoneyModel,
                       [grid, chart],
                       "Money Model",
                       {"N": 100, "width": 10, "height": 10})
server.port = 8521  # The default
server.launch()

