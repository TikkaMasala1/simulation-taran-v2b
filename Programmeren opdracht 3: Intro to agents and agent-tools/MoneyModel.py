import matplotlib.pyplot as plt
import numpy as np
from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid
from mesa.datacollection import DataCollector
from mesa.batchrunner import BatchRunner


def compute_gini(model):
    agent_wealths = [agent.wealth for agent in model.schedule.agents]
    x = sorted(agent_wealths)
    N = model.num_agents
    B = sum(xi * (N - i) for i, xi in enumerate(x)) / (N * sum(x))
    return (1 + (1 / N) - 2 * B)


class MoneyAgent(Agent):
    """ An agent with fixed initial wealth."""

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.wealth = 1

    def move(self):
        possible_steps = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center=False)
        new_position = self.random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)

    def give_money(self):
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        if len(cellmates) > 1:
            other = self.random.choice(cellmates)
            other.wealth += 1
            self.wealth -= 1

    def step(self):
        self.move()
        if self.wealth > 0:
            self.give_money()


class MoneyModel(Model):
    """A model with some number of agents."""

    def __init__(self, N, width, height):
        self.num_agents = N
        self.grid = MultiGrid(width, height, True)
        self.schedule = RandomActivation(self)
        self.running = True

        # Create agents
        for i in range(self.num_agents):
            a = MoneyAgent(i, self)
            self.schedule.add(a)
            # Add the agent to a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))

        self.datacollector = DataCollector(
            model_reporters={"Gini": compute_gini},
            agent_reporters={"Wealth": "wealth"})

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()


model = MoneyModel(50, 10, 10)
for i in range(100):
    model.step()

# Gets the series of Gini coefficients as a pandas DataFrame
gini = model.datacollector.get_model_vars_dataframe()
# Visualising the data
gini.plot()
plt.show()

# Gets the agent wealth data
agent_wealth = model.datacollector.get_agent_vars_dataframe()
# Prints the data
print(agent_wealth.head())

# Gets the agent wealth at the model's end
end_wealth = agent_wealth.xs(99, level="Step")["Wealth"]
# Visualising the data
end_wealth.hist(bins=range(agent_wealth.Wealth.max() + 1)).plot()
plt.show()

# Gets the wealth of a given agent (in this example, agent 14):
one_agent_wealth = agent_wealth.xs(14, level="AgentID")
# Visualising the data
one_agent_wealth.Wealth.plot()
plt.show()

# Setting the fixed parameters
fixed_params = {"width": 10,
                "height": 10}

# Setting the variable parameters
# Varying the number of agents
variable_params = {"N": range(10, 500, 10)}

# Setting up the BatchRunner
batch_run = BatchRunner(MoneyModel,
                        variable_params,
                        fixed_params,
                        iterations=5,
                        max_steps=100,
                        model_reporters={"Gini": compute_gini})
# Running the BatchRunner
batch_run.run_all()

# Passing model collection via BatchRunner
run_data = batch_run.get_model_vars_dataframe()
run_data.head()
# Visualising the data
plt.scatter(run_data.N, run_data.Gini)
plt.show()


# Geting the Agent DataCollection
data_collector_agents = batch_run.get_collector_agents()
print(data_collector_agents[(10, 2)])


# Geting the Model DataCollection.
data_collector_model = batch_run.get_collector_model()
print(data_collector_model[(10, 1)])
